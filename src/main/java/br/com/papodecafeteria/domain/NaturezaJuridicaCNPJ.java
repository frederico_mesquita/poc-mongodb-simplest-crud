package br.com.papodecafeteria.domain;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import com.mongodb.BasicDBObjectBuilder;
import com.mongodb.DBObject;

import br.com.papodecafeteria.domain.vldt.Vldt;

public class NaturezaJuridicaCNPJ extends Vldt implements Serializable {
	private static final long serialVersionUID = 1L;
	private static final Logger l = Logger.getLogger(NaturezaJuridicaCNPJ.class.getName());

	public NaturezaJuridicaCNPJ(){
		super();
	}
	
	private NaturezaJuridicaCNPJ(int pKey, String pCodigo, String pNaturezaJuridica,
									String pRepresentante, String pQualificacao){
		super();
		try{
			setiKey(pKey);
			setcCodigo(pCodigo);
			setcNaturezaJuridica(pNaturezaJuridica);
			setcRepresentante(pRepresentante);
			setcQualificacao(pQualificacao);
		} catch (Exception exc) {
			l.log(Level.SEVERE, exc.getMessage(), exc);
		}
	}
	
	public static NaturezaJuridicaCNPJ getNaturezaJuridicaCNPJ(int pKey, String pCodigo, String pNaturezaJuridica,
			String pRepresentante, String pQualificacao){
		NaturezaJuridicaCNPJ pNaturezaJuridicaCNPJ = null;
		try{
			pNaturezaJuridicaCNPJ = new NaturezaJuridicaCNPJ(pKey, pCodigo, pNaturezaJuridica, pRepresentante, pQualificacao);
			validate(pNaturezaJuridicaCNPJ);
		} catch (Exception exc) {
			l.log(Level.SEVERE, exc.getMessage(), exc);
		}
		return pNaturezaJuridicaCNPJ;
	}
	
	@NotNull
	private int iKey;
	
	@NotNull @Pattern(regexp = "\\d{3}-\\d{1}")
	private String cCodigo;
	
	@NotNull @Size(min = 10, max = 200)
	private String cNaturezaJuridica;
	
	@NotNull @Size(min = 10, max = 200)
	private String cRepresentante;
	
	@NotNull @Pattern(regexp = "\\d{2}")
	private String cQualificacao;
	
	public DBObject getNaturezaJuridicaCNPJDBObject(){
		BasicDBObjectBuilder pBasicDBObjectBuilder = null;
		try {
			pBasicDBObjectBuilder = BasicDBObjectBuilder.start();
			pBasicDBObjectBuilder.add("key", getiKey());
			pBasicDBObjectBuilder.add("codigo", getcCodigo());
			pBasicDBObjectBuilder.add("naturezaJuridica", getcNaturezaJuridica());
			pBasicDBObjectBuilder.add("representante", getcRepresentante());
			pBasicDBObjectBuilder.add("qualificacao", getcQualificacao());
		} catch (Exception exc) {
			l.log(Level.SEVERE, exc.getMessage(), exc);
		}
		return pBasicDBObjectBuilder.get();
	}
	
	@Override
	public String toString(){
		String  cReturn = "";
		try{
			cReturn += getNaturezaJuridicaCNPJDBObject().toString(); 
		} catch (Exception exc) {
			l.log(Level.SEVERE, exc.getMessage(), exc);
		}
		return cReturn;
	}

	public int getiKey() {
		return iKey;
	}
	public void setiKey(int iKey) {
		this.iKey = iKey;
	}
	public String getcCodigo() {
		return cCodigo;
	}
	public void setcCodigo(String cCodigo) {
		this.cCodigo = cCodigo;
	}
	public String getcNaturezaJuridica() {
		return cNaturezaJuridica;
	}
	public void setcNaturezaJuridica(String cNaturezaJuridica) {
		this.cNaturezaJuridica = cNaturezaJuridica;
	}
	public String getcRepresentante() {
		return cRepresentante;
	}
	public void setcRepresentante(String cRepresentante) {
		this.cRepresentante = cRepresentante;
	}
	public String getcQualificacao() {
		return cQualificacao;
	}
	public void setcQualificacao(String cQualificacao) {
		this.cQualificacao = cQualificacao;
	}
}
