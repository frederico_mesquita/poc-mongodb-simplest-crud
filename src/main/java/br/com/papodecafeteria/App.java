package br.com.papodecafeteria;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

import com.mongodb.BasicDBObjectBuilder;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.WriteResult;

import br.com.papodecafeteria.domain.Address;
import br.com.papodecafeteria.domain.CNAE;
import br.com.papodecafeteria.domain.CNPJ;
import br.com.papodecafeteria.domain.ContactInformation;
import br.com.papodecafeteria.domain.Dimensoes;
import br.com.papodecafeteria.domain.NaturezaJuridicaCNPJ;
import br.com.papodecafeteria.domain.Phone;
import br.com.papodecafeteria.domain.Product;
import br.com.papodecafeteria.domain.ProductCategory;
import br.com.papodecafeteria.domain.conn.MongoDBConn;

public class App {
    public static void main( String[] args ){
        System.out.println( "Hello MongoDB!" );
        MongoDBConn pMongoDBConn = null;
        try{
        	pMongoDBConn = MongoDBConn.getMongoDBConn("localhost", 27017);
        	
            @SuppressWarnings("deprecation")
			DB database = pMongoDBConn.getMongoClient().getDB("stock");
            DBCollection dbCollection = database.getCollection("product");
            
            @SuppressWarnings("unused")
			WriteResult pWriteResult = dbCollection.insert(getFilledProduct().getProductDBObject());

            DBObject query = BasicDBObjectBuilder.start().add("key", 0).get();
    		DBCursor cursor = dbCollection.find(query);
    		while(cursor.hasNext())
    			System.out.println(cursor.next());
            
    		pWriteResult = dbCollection.update(query, getFilledUpdatedProduct().getProductDBObject());
    		cursor = dbCollection.find(query);
    		while(cursor.hasNext())
    			System.out.println(cursor.next());
    		
    		dbCollection.remove(query);
        }catch (Exception e) {
			System.out.println(e.getMessage());
		}
        pMongoDBConn.getMongoClient().close();
    }
    
    private static Product getFilledUpdatedProduct(){
    	Product pProduct = null;
        try{
        	CNPJ suplier = CNPJ.getCNPJ(0, (new Date()), "Empresa Brasileira de Produtos S/A - Updated", "Xulambs da Serra", 
        							Arrays.asList(
        								CNAE.getCNAE(0, "2543-8/00", "ALAVANCAS E P�S-DE-CABRA; FABRICA��O DE"),
        								CNAE.getCNAE(0, "2543-8/00", "ANCINHOS MANUAIS; FABRICA��O DE")), 
        							Arrays.asList(
        								CNAE.getCNAE(0, "2543-8/00", "ALAVANCAS E P�S-DE-CABRA; FABRICA��O DE")), 
        							NaturezaJuridicaCNPJ.getNaturezaJuridicaCNPJ(
        								0, "209-7", "Sociedade Empres�ria em Comandita por A��es", "Diretor ou Presidente", "16"), 
        							Arrays.asList(
        								ContactInformation.getContactInformation(
        									0, 
        									Address.getAddress(0, "Nome da rua - Updated", "8", "00000-000", ""), 
        									Phone.getPhone(0, 11, "00000-0000"), 
        									"")
        							), 
        							"00000000/0000-00");
        	
            pProduct = Product.getProduct(
				0, 
				"Nome do Produto", 
				"Descri��o do produto", 
				Dimensoes.getDimensoes(
							2.45, 10.0, 7.0, "cm", 
							90.0, 0.10, "Kg"), 
				new ArrayList(Arrays.asList("ANS-011 Norma sanit�ria - Updated")), 
				new ArrayList(Arrays.asList("path/img.ext - Updated")), 
				"Composi��o do produto", 
				ProductCategory.getProductCategory(
									0, 
									"abc", 
									"Descri��o da categoria do produto - Updated")
				, Arrays.asList(suplier));
        }catch (Exception e) {
			System.out.println(e.getMessage());
		}
        return pProduct;
    }
    
    private static Product getFilledProduct(){
    	Product pProduct = null;
        try{
        	CNPJ suplier = CNPJ.getCNPJ(0, (new Date()), "Empresa Brasileira de Produtos S/A", "Xulambs da Serra", 
        							Arrays.asList(
        								CNAE.getCNAE(0, "2543-8/00", "ALAVANCAS E P�S-DE-CABRA; FABRICA��O DE"),
        								CNAE.getCNAE(0, "2543-8/00", "ANCINHOS MANUAIS; FABRICA��O DE")), 
        							Arrays.asList(
        								CNAE.getCNAE(0, "2543-8/00", "ALAVANCAS E P�S-DE-CABRA; FABRICA��O DE"),
        								CNAE.getCNAE(0, "2543-8/00", "ANCINHOS MANUAIS; FABRICA��O DE")), 
        							NaturezaJuridicaCNPJ.getNaturezaJuridicaCNPJ(
        								0, "209-7", "Sociedade Empres�ria em Comandita por A��es", "Diretor ou Presidente", "16"), 
        							Arrays.asList(
        								ContactInformation.getContactInformation(
        									0, 
        									Address.getAddress(0, "Nome da rua", "8", "00000-000", ""), 
        									Phone.getPhone(0, 11, "00000-0000"), 
        									"")
        							), 
        							"00000000/0000-00");
        	
            pProduct = Product.getProduct(
				0, 
				"Nome do Produto", 
				"Descri��o do produto", 
				Dimensoes.getDimensoes(
							2.45, 10.0, 7.0, "cm", 
							90.0, 0.10, "g"), 
				new ArrayList(Arrays.asList("ANS-011 Norma sanit�ria")), 
				new ArrayList(Arrays.asList("path/img.ext")), 
				"Composi��o do produto", 
				ProductCategory.getProductCategory(
									0, 
									"abc", 
									"Descri��o da categoria do produto")
				, Arrays.asList(suplier));
        }catch (Exception e) {
			System.out.println(e.getMessage());
		}
        return pProduct;
    }
}
