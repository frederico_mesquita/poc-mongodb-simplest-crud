package br.com.papodecafeteria.domain;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

import com.mongodb.BasicDBObjectBuilder;
import com.mongodb.DBObject;

import br.com.papodecafeteria.domain.vldt.Vldt;

public class ContactInformation extends Vldt implements Serializable{
	private static final long serialVersionUID = 1L;
	private static final Logger l = Logger.getLogger(ContactInformation.class.getName());

	public ContactInformation(){
		super();
	}
	
	private ContactInformation(int pKey, Address pAdderss, Phone pPhone, String pEmail){
		super();
		try{
			setiKey(pKey);
			setAddress(pAdderss);
			setPhone(pPhone);
			setcEmail(pEmail);
		}catch (Exception exc) {
			l.log(Level.SEVERE, exc.getMessage(), exc);
		}
	}
	
	public static ContactInformation getContactInformation(int pKey, Address pAdderss, Phone pPhone, String pEmail){
		ContactInformation pContactInformation = null;
		try{
			pContactInformation = new ContactInformation(pKey, pAdderss, pPhone, pEmail);
			validate(pContactInformation);
		}catch (Exception exc) {
			l.log(Level.SEVERE, exc.getMessage(), exc);
		}
		return pContactInformation;
	}
	
	@NotNull
	private int iKey;
	
	@NotNull @Valid
	private Address address;
	
	@NotNull @Valid
	private Phone phone;
	
	@NotNull @Email 
	private String cEmail;
	
	public DBObject getContactInformationDBObject(){
		BasicDBObjectBuilder pBasicDBObjectBuilder = null;
		try {
			pBasicDBObjectBuilder = BasicDBObjectBuilder.start();
			pBasicDBObjectBuilder.add("key", getiKey());
			pBasicDBObjectBuilder.add("address", getAddress().toString());
			pBasicDBObjectBuilder.add("phone", getPhone().toString());
			pBasicDBObjectBuilder.add("email", getcEmail());
		} catch (Exception exc) {
			l.log(Level.SEVERE, exc.getMessage(), exc);
		}
		return pBasicDBObjectBuilder.get();
	}
	
	@Override
	public String toString(){
		String cReturn = "";
		try{
			cReturn += getContactInformationDBObject().toString();
		}catch (Exception exc) {
			l.log(Level.SEVERE, exc.getMessage(), exc);
		}
		return cReturn;
	}

	public int getiKey() {
		return iKey;
	}
	public void setiKey(int iKey) {
		this.iKey = iKey;
	}
	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}
	public Phone getPhone() {
		return phone;
	}
	public void setPhone(Phone phone) {
		this.phone = phone;
	}
	public String getcEmail() {
		return cEmail;
	}
	public void setcEmail(String cEmail) {
		this.cEmail = cEmail;
	}	
}
