package br.com.papodecafeteria.domain.conn;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.mongodb.MongoClient;

public class MongoDBConn implements Serializable {
	private static final long serialVersionUID = 1L;
	private static final Logger l = Logger.getLogger(MongoDBConn.class.getName());  
	
	public MongoDBConn(){
		super();
	}
	
	private MongoDBConn(String pServer, int pPort){
		super();
		try {
			setMongoClient(new MongoClient( pServer , pPort ));
		} catch (Exception exc) {
			l.log(Level.SEVERE, exc.getMessage(), exc);
		}
	}
	
	public static MongoDBConn getMongoDBConn(String pServer, int pPort){
		MongoDBConn pMongoDBConn = null;
		try {
			pMongoDBConn = new MongoDBConn(pServer, pPort);
		} catch (Exception exc) {
			l.log(Level.SEVERE, exc.getMessage(), exc);
		}
		return pMongoDBConn;
	}
	
	private String server;
	private int port;
	private MongoClient mongoClient = null;

	public String getServer() {
		return server;
	}
	public void setServer(String server) {
		this.server = server;
	}
	public int getPort() {
		return port;
	}
	public void setPort(int port) {
		this.port = port;
	}
	public MongoClient getMongoClient() {
		return mongoClient;
	}
	public void setMongoClient(MongoClient mongoClient) {
		this.mongoClient = mongoClient;
	}
}
