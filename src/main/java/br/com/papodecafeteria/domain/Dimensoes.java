package br.com.papodecafeteria.domain;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

import com.mongodb.BasicDBObjectBuilder;
import com.mongodb.DBObject;

import br.com.papodecafeteria.domain.vldt.Vldt;

public class Dimensoes extends Vldt implements Serializable {
	private static final long serialVersionUID = 1L;
	private static final Logger l = Logger.getLogger(Dimensoes.class.getName());
	
	public Dimensoes(){
		super();
	}
	
	private Dimensoes(double pLargura, double pAltura, double pComprimento, String pUnidMedidaVolume,
						double pPesoLiquido, double pPesoEmbalagem, String pUnidMedidaPeso){
		super();
		try{
			setLargura(pLargura);
			setAltura(pAltura);
			setComprimento(pComprimento);
			setUnidMedidaVolume(pUnidMedidaVolume);
			setPesoLiquido(pPesoLiquido);
			setPesoEmbalagem(pPesoEmbalagem);
			setUnidMedidaPeso(pUnidMedidaPeso);
		}catch (Exception exc) {
			l.log(Level.SEVERE, exc.getMessage(), exc);
		}
	}
	
	public static Dimensoes getDimensoes(
							double pLargura, double pAltura, double pComprimento, String pUnidMedidaVolume,
							double pPesoLiquido, double pPesoEmbalagem, String pUnidMedidaPeso){
		Dimensoes pDimensoes = null;
		try{
			pDimensoes = new Dimensoes(
								pLargura, pAltura, pComprimento, pUnidMedidaVolume, 
								pPesoLiquido, pPesoEmbalagem, pUnidMedidaPeso);
			validate(pDimensoes);
		}catch (Exception exc) {
			l.log(Level.SEVERE, exc.getMessage(), exc);
		}
		return pDimensoes;
	}
	
	@NotNull
	private int iKey;
	
	@NotNull @Positive
	private double largura;
	
	@NotNull @Positive
	private double altura;
	
	@NotNull @Size(min = 1, max = 10)
	private String unidMedidaVolume;
	
	@NotNull @Positive
	private double comprimento;
	
	@NotNull @Positive
	private double pesoLiquido;
	
	@NotNull @Positive
	private double pesoEmbalagem;
	
	@NotNull @Size(min = 1, max = 10)
	private String unidMedidaPeso;
	
	public DBObject getDimensoesDBObject(){
		BasicDBObjectBuilder pBasicDBObjectBuilder = null;
		try {
			pBasicDBObjectBuilder = BasicDBObjectBuilder.start();
			pBasicDBObjectBuilder.add("key", getiKey());
			pBasicDBObjectBuilder.add("largura", getLargura());
			pBasicDBObjectBuilder.add("altura", getAltura());
			pBasicDBObjectBuilder.add("unidMedidaVolume", getUnidMedidaVolume());
			pBasicDBObjectBuilder.add("comprimento", getComprimento());
			pBasicDBObjectBuilder.add("pesoLiquido", getPesoLiquido());
			pBasicDBObjectBuilder.add("pesoEmbalagem", getPesoEmbalagem());
			pBasicDBObjectBuilder.add("unidMedidaPeso", getUnidMedidaPeso());
		} catch (Exception exc) {
			l.log(Level.SEVERE, exc.getMessage(), exc);
		}
		return pBasicDBObjectBuilder.get();
	}
	
	@Override
	public String toString(){
		String cReturn = "";
		try{
			cReturn += getDimensoesDBObject().toString();
		}catch (Exception exc) {
			l.log(Level.SEVERE, exc.getMessage(), exc);
		}
		return cReturn;
	}

	public double getLargura() {
		return largura;
	}
	public void setLargura(double largura) {
		this.largura = largura;
	}
	public double getAltura() {
		return altura;
	}
	public void setAltura(double altura) {
		this.altura = altura;
	}
	public double getComprimento() {
		return comprimento;
	}
	public void setComprimento(double comprimento) {
		this.comprimento = comprimento;
	}
	public double getPesoLiquido() {
		return pesoLiquido;
	}
	public void setPesoLiquido(double pesoLiquido) {
		this.pesoLiquido = pesoLiquido;
	}
	public double getPesoEmbalagem() {
		return pesoEmbalagem;
	}
	public void setPesoEmbalagem(double pesoEmbalagem) {
		this.pesoEmbalagem = pesoEmbalagem;
	}
	public int getiKey() {
		return iKey;
	}
	public void setiKey(int iKey) {
		this.iKey = iKey;
	}
	public String getUnidMedidaVolume() {
		return unidMedidaVolume;
	}
	public void setUnidMedidaVolume(String unidMedidaVolume) {
		this.unidMedidaVolume = unidMedidaVolume;
	}
	public String getUnidMedidaPeso() {
		return unidMedidaPeso;
	}
	public void setUnidMedidaPeso(String unidMedidaPeso) {
		this.unidMedidaPeso = unidMedidaPeso;
	}
}
